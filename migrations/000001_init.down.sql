DROP TABLE order_products;

DROP TABLE orders;

DROP TABLE delivery_tarif_values;

DROP TABLE delivery_tarifs;

DROP TYPE order_type;

DROP TYPE order_status;

DROP TYPE order_payment_type;

DROP TYPE delivery_tarif_type;

DROP TRIGGER IF EXISTS generate_order_id_trigger ON "orders";

DROP FUNCTION IF EXISTS generate_order_id();

