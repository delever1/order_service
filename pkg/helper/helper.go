package helper

import (
	"fmt"
	"sync"
	"time"
)

// CustomIDGenerator represents an ID generator with a specific format.
type CustomIDGenerator struct {
	mu sync.Mutex
}

// NewCustomIDGenerator creates a new CustomIDGenerator with default values.
func NewCustomIDGenerator() *CustomIDGenerator {
	return &CustomIDGenerator{}
}

// GenerateID generates a unique custom ID using a timestamp and a counter.
func (gen *CustomIDGenerator) GenerateID() string {
	gen.mu.Lock()
	defer gen.mu.Unlock()

	timestamp := time.Now().UnixMilli()
	id := fmt.Sprintf("%d", timestamp)

	id = id[5:11]

	return id
}
