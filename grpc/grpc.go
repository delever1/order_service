package grpc

import (
	"delever/order_service/config"
	"delever/order_service/genproto/order_service"
	"delever/order_service/grpc/client"
	"delever/order_service/grpc/service"
	"delever/order_service/pkg/logger"
	"delever/order_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
